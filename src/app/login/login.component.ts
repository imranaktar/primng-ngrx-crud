import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ApiService } from './../services/api.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: any;
    submitted = false;

    constructor(private apiService: ApiService, public router: Router, public http: HttpClient) { }


    ngOnInit() {
        this.loginForm = new FormGroup({
            'email': new FormControl('', Validators.required),
            'password': new FormControl('', Validators.required)
        });
    }

    checkToken() {
        let token = localStorage.getItem('token')
        if (token) {
            this.router.navigateByUrl('/')
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: "Already LogedIn...!",
            })
            this.router.navigateByUrl('/users')
        }
    }

    onSubmit() {
        this.submitted = true;
        this.apiService.add(`users/login`, this.loginForm.value).subscribe((resp: any) => {
            if (resp.type === 'success') {
                Swal.fire({
                    icon: 'success',
                    title: 'Done.!',
                    text: resp.message,
                });
                localStorage.setItem('token', resp.data)
                this.router.navigateByUrl('/users')
            } else {
                Swal.fire({
                    icon: 'error',
                    text: resp.message,
                });
            }
        })
    }
}
