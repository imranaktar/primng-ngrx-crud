import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsersCreateComponent } from './components/users-create/users-create.component'
import { UsersDetailComponent } from './components/users-detail/users-detail.component'
import { UsersEditComponent } from './components/users-edit/users-edit.component'
import { UsersListComponent } from './components/users-list/users-list.component'
import { UsersComponent } from './users.component'

const routes: Routes = [
    {
        path: '',
        component: UsersComponent,
        children: [
            { path: '', component: UsersListComponent },
            { path: 'detail/:id', component: UsersDetailComponent },
            { path: 'create', component: UsersCreateComponent },
            { path: 'edit/:id', component: UsersEditComponent }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersRoutingModule { }
