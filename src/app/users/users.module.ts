import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { StoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from './../store/users.effects';
import * as userReducer from './../store/users.reducers';
import { UsersService } from '../services/users.service';
import { UsersDetailComponent } from './components/users-detail/users-detail.component';
import { UsersCreateComponent } from './components/users-create/users-create.component';
import { UsersEditComponent } from './components/users-edit/users-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { PanelModule } from 'primeng/panel';

export const reducers: ActionReducerMap<any> = {
  users: userReducer.reducer,
};

@NgModule({
  declarations: [UsersComponent, UsersListComponent, UsersDetailComponent, UsersCreateComponent, UsersEditComponent],
  imports: [
    PanelModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    UsersRoutingModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([UserEffects])
  ],
  exports: [],
  providers: [UsersService]
})
export class UsersModule { }
