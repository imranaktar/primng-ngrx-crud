import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { User } from './../../../shared/custome-types/user';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from './../../../shared/state/app.state';
import * as userActions from './../../../store/users.reducers';
import { GetUser, UpdateUser } from './../../../store/users.actions';
import { getUser } from './../../../store/users.reducers';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.scss']
})
export class UsersEditComponent implements OnInit {
  user: User | any;
  name: any;
  city: any;
  age: any;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>) {

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.store.dispatch(new GetUser(+params.id));
    });
    this.store.select(getUser).subscribe(user => {
      if (user != null) {
        this.user = user;
        this.name = user.name;
        this.city = user.city;
        this.age = user.age;
      }
    });
  }



  /**
   * Create a new user
   */
  onSaveUser() {
    let updatedUser = { ...this.user, name: this.name, city: this.city, age: this.age }
    this.store.dispatch(new UpdateUser(updatedUser));
    this.router.navigate(['/users']);
  }
  /**
   * If user is in view mode, back to edit mode else go to users page
   */
  onBack() {
    this.router.navigate(['/users']);
  }

  /**
   * Reset all fields in the form
   */
  reset() {
    this.user.name = '';
    this.user.city = '';
    this.user.age = 0;
  }
}
