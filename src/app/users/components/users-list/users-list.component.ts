import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as userActions from './../../../store/users.actions';
import { User } from './../../../shared/custome-types/user';
import { AppState } from './../../../shared/state/app.state';
import { getAllUsers } from './../../../store/users.reducers';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  title = 'List of Users';
  users: Observable<User[]> | undefined;
  isLoading: boolean = true;

  products: any[] | undefined;
  cols: any[] | undefined;

  constructor(private store: Store<AppState>, public router: Router) { }

  ngOnInit(): void {
    this.users = this.store.select(getAllUsers);
    this.cols = [
      { field: 'id', header: 'ID' },
      { field: 'name', header: 'Name' },
      { field: 'city', header: 'City' },
      { field: 'age', header: 'Age' }
    ];
    this.isLoading = false;
  }

  delete(id: any) {
    if (confirm('Are you sure do you want to delete this User?')) {
      this.store.dispatch(new userActions.RemoveUser(id));
    }
  }

  logout() {
    localStorage.removeItem('token')
    this.router.navigateByUrl('/')
  }

}
