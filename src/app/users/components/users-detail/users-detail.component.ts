import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './../../../shared/custome-types/user';
import { Store } from '@ngrx/store';
import { AppState } from './../../../shared/state/app.state';
import { ActivatedRoute } from '@angular/router';
import { GetUser } from './../../../store/users.actions';
import { getUser } from './../../../store/users.reducers';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.scss']
})
export class UsersDetailComponent implements OnInit {
  user: Observable<User> | undefined;
  constructor(
    private route: ActivatedRoute,
    private store: Store<AppState>
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch(new GetUser(+params.id));
    });
    this.user = this.store.select(getUser);
  }

}
