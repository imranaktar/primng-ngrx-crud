import { Component, OnInit } from '@angular/core';
import { User } from './../../../shared/custome-types/user';
import { AppState } from './../../../shared/state/app.state';
import { Store } from '@ngrx/store';
import { AddUser } from './../../../store/users.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.scss']
})
export class UsersCreateComponent implements OnInit {
  user: User = new User();
  age : any;
  name : any;
  city : any;
  constructor(private router: Router,
    private store: Store<AppState>) {

  }

  ngOnInit() {
  }

  /**
  * If user is in view mode, back to edit mode else go to users page
  */
  onBack() {
    this.router.navigate(['/users']);
  }

  /**
  * Create a new hero
  */
  onSaveUser() {
    this.store.dispatch(new AddUser(this.user));
    this.router.navigate(['/users']);
  }

  reset() {
    this.user.name = '';
    this.user.city = '';
    this.user.age = 0;
  }


}
