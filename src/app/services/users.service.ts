import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './../shared/custome-types/user';

@Injectable()
export class UsersService {
    protected URL = 'http://localhost:3000/api/users';

    constructor(protected http: HttpClient) {
    }

    /**
     * Find an object by its identifier
     * @param id the object identifier
     * @returns gets the object found
     */
    public findById(id: any): Observable<User> {
        return this.http.get<User>(this.URL + '/' + id);
    }

    /**
     * Find all the elements
     * @returns gets the list of objects found
     */
    public findAll(params?: any): Observable<User[]> {
        return this.http.get<User[]>(this.URL, { params });
    }

    /**
     * Delete an object by its identifier field
     * @param id the object identifier
     * @returns gets the response
     */
    public delete(id: any): Observable<User> {
        return this.http.delete<User>(this.URL + '/' + id);
    }

    /**
     * Insert the data
     * @param data the object containing the data to be inserted
     * @returns gets the response
     */
    public insert(data: User): Observable<User> {
        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.post<User>(this.URL, data, { headers });
    }

    /**
     * Update specific object into DB
     * @param user the object to be updated
     * @returns gets the response
     */
    public update(user: User): Observable<User> {
        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json; charset=utf-8');

        return this.http.put<User>(this.URL + '/' + user.id, user, { headers });
    }
}
