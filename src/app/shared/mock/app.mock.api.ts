import { InMemoryDbService } from 'angular-in-memory-web-api';

export class AppMockApi implements InMemoryDbService {
  createDb() {
    return {
      users: [
        {
          id: 1,
          name: 'Faisal Saleem',
          city: 'Sheikhupura',
          age: 26,
        },
        {
          id: 2,
          name: 'Saad Saleem',
          city: 'Lahore',
          age: 20,
        },
        {
          id: 3,
          name: 'Usama',
          city: 'Lahore',
          age: 55,
        },
        {
          id: 4,
          name: 'Zubair',
          city: 'Multan',
          age: 36,
        },
        {
          id: 5,
          name: 'Usman',
          city: 'Lahore',
          age: 6,
        },
      ],
    };
  }
}
