import * as fromUsers from './../../store/users.reducers';

export interface AppState {
    users: fromUsers.State;
}
