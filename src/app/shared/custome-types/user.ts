export class User {
    id?: number;
    name?: string;
    city?: string;
    age?: number;
}