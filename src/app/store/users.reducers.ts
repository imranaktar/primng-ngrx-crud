import * as usersActions from './users.actions';
import { AppAction } from './../shared/state/app.action';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { User } from './../shared/custome-types/user';

export interface State {
    data: User[];
    selected: any;
    action: any;
    done: boolean;
    error?: any;
}

const initialState: State = {
    data: [],
    selected: null,
    action: null,
    done: false,
    error: null,
};

export function reducer(state = initialState, action: AppAction): State {
    // ...state create immutable state object
    switch (action.type) {
        /*************************
         * GET all users actions
         ************************/
        case usersActions.GET_USERS:
            return {
                ...state,
                action: usersActions.GET_USERS,
                done: false,
                selected: null,
                error: null,
            };
        case usersActions.GET_USERS_SUCCESS:
            return {
                ...state,
                data: action.payload,
                done: true,
                selected: null,
                error: null,
            };
        case usersActions.GET_USERS_ERROR:
            return {
                ...state,
                done: true,
                selected: null,
                error: action.payload,
            };

        /*************************
         * GET user by id actions
         ************************/
        case usersActions.GET_USER:
            return {
                ...state,
                action: usersActions.GET_USER,
                done: false,
                selected: null,
                error: null,
            };
        case usersActions.GET_USER_SUCCESS:
            return {
                ...state,
                selected: action.payload,
                done: true,
                error: null,
            };
        case usersActions.GET_USER_ERROR:
            return {
                ...state,
                selected: null,
                done: true,
                error: action.payload,
            };

        /*************************
         * CREATE user actions
         ************************/
        case usersActions.CREATE_USER:
            return {
                ...state,
                selected: action.payload,
                action: usersActions.CREATE_USER,
                done: false,
                error: null,
            };
        case usersActions.CREATE_USER_SUCCESS: {
            const newUser = {
                ...state.selected,
                id: action.payload,
            };
            const data = [...state.data, newUser];
            return {
                ...state,
                data,
                selected: null,
                error: null,
                done: true,
            };
        }
        case usersActions.CREATE_USER_ERROR:
            return {
                ...state,
                selected: null,
                done: true,
                error: action.payload,
            };

        /*************************
         * UPDATE user actions
         ************************/
        case usersActions.UPDATE_USER:
            return {
                ...state,
                selected: action.payload,
                action: usersActions.UPDATE_USER,
                done: false,
                error: null,
            };
        case usersActions.UPDATE_USER_SUCCESS: {
            const index = state.data.findIndex((h) => h.id === state.selected.id);
            if (index >= 0) {
                const data = [
                    ...state.data.slice(0, index),
                    state.selected,
                    ...state.data.slice(index + 1),
                ];
                return {
                    ...state,
                    data,
                    done: true,
                    selected: null,
                    error: null,
                };
            }
            return state;
        }
        case usersActions.UPDATE_USER_ERROR:
            return {
                ...state,
                done: true,
                selected: null,
                error: action.payload,
            };

        /*************************
         * DELETE user actions
         ************************/
        case usersActions.DELETE_USER: {
            const selected = state.data.find((h) => h.id === action.payload);
            return {
                ...state,
                selected,
                action: usersActions.DELETE_USER,
                done: false,
                error: null,
            };
        }
        case usersActions.DELETE_USER_SUCCESS: {
            const data = state.data.filter((h) => h.id !== state.selected.id);
            return {
                ...state,
                data,
                selected: null,
                error: null,
                done: true,
            };
        }
        case usersActions.DELETE_USER_ERROR:
            return {
                ...state,
                selected: null,
                done: true,
                error: action.payload,
            };
    }
    return state;
}

/*************************
 * SELECTORS
 ************************/
export const getUsersState = createFeatureSelector<State>('users');
export const getAllUsers = createSelector(
    getUsersState,
    (state: State) => state.data
);
export const getUser = createSelector(getUsersState, (state: State) => {
    if (state.action === usersActions.GET_USER && state.done) {
        return state.selected;
    } else {
        return null;
    }
});
export const isDeleted = createSelector(
    getUsersState,
    (state: State) =>
        state.action === usersActions.DELETE_USER && state.done && !state.error
);
export const isCreated = createSelector(
    getUsersState,
    (state: State) =>
        state.action === usersActions.CREATE_USER && state.done && !state.error
);
export const isUpdated = createSelector(
    getUsersState,
    (state: State) =>
        state.action === usersActions.UPDATE_USER && state.done && !state.error
);

export const getDeleteError = createSelector(getUsersState, (state: State) => {
    return state.action === usersActions.DELETE_USER ? state.error : null;
});
export const getCreateError = createSelector(getUsersState, (state: State) => {
    return state.action === usersActions.CREATE_USER ? state.error : null;
});
export const getUpdateError = createSelector(getUsersState, (state: State) => {
    return state.action === usersActions.UPDATE_USER ? state.error : null;
});
export const getUsersError = createSelector(getUsersState, (state: State) => {
    return state.action === usersActions.GET_USERS ? state.error : null;
});
export const getUserError = createSelector(getUsersState, (state: State) => {
    return state.action === usersActions.GET_USER ? state.error : null;
});
