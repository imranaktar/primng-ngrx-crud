import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as userActions from './users.actions';
import {
    AddUser,
    AddUsersuccess,
    AddUserError,
    RemoveUser,
    RemoveUsersuccess,
    RemoveUserError,
    UpdateUser,
    UpdateUsersuccess,
    UpdateUserError,
    GetAllUsers,
    GetAllUsersSuccess,
    GetAllUsersError,
    GetUser,
    GetUsersuccess,
    GetUserError,
} from './users.actions';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { UsersService } from './../services/users.service';
import { User } from './../shared/custome-types/user';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class UserEffects {
    constructor(private actions$: Actions,
        private svc: UsersService) {
    }

    @Effect()
    getAllUsers$: Observable<Action> = this.actions$.pipe(
        ofType(userActions.GET_USERS),
        switchMap(() => this.svc.findAll()),
        map(heroes => new GetAllUsersSuccess(heroes)),
        catchError((err) => [new GetAllUsersError(err)])
    );

    @Effect()
    getUser$ = this.actions$.pipe(
        ofType(userActions.GET_USER),
        map((action: GetUser) => action.payload),
        switchMap(id => this.svc.findById(id)),
        map(hero => new GetUsersuccess(hero)),
        catchError((err) => [new GetUserError(err)])
    );


    @Effect()
    updateUser$ = this.actions$.pipe(
        ofType(userActions.UPDATE_USER),
        map((action: UpdateUser) => action.payload),
        switchMap(user => this.svc.update(user)),
        map(() => new UpdateUsersuccess()),
        catchError((err) => [new UpdateUserError(err)])
    );

    @Effect()
    createUser$ = this.actions$.pipe(
        ofType(userActions.CREATE_USER),
        map((action: AddUser) => action.payload),
        switchMap(newUser => this.svc.insert(newUser)),
        map((response: any) => new AddUsersuccess(response.id)),
        catchError((err) => [new AddUserError(err)])
    );

    @Effect()
    removeUser$ = this.actions$.pipe(
        ofType(userActions.DELETE_USER),
        map((action: RemoveUser) => action.payload),
        switchMap(id => this.svc.delete(id)),
        map((hero: User) => new RemoveUsersuccess(hero)),
        catchError((err) => [new RemoveUserError(err)])
    );
}
