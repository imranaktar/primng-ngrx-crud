import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AppMockApi } from './shared/mock/app.mock.api';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { PanelModule } from 'primeng/panel';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    ButtonModule,
    InputTextModule,
    PanelModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(AppMockApi, { delay: 1000, passThruUnknownUrl: true }),
    // 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
